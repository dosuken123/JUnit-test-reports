class StringHelper
  def concatenate(a, b)
    raise ArgumentError unless a.is_a?(String) && b.is_a?(String)

    a - b
  end
end
