require 'hash_scan'

describe HashScan do
  describe '#scan' do
    subject { described_class.new.scan(hash, key) }

    context 'when argument is hash' do
      let(:hash) { { 'git' => 'lab' } }
      let(:key) { 'git' }

      it 'returns the value' do
        is_expected.to eq('lab')
      end
    end

    context 'when argument is not hash' do
      let(:hash) { 'git' }
      let(:key) { 'git' }

      it 'raises and error' do
        expect { subject }.to raise_error(ArgumentError)
      end
    end
  end
end
