require 'test'

describe Test do
  describe '#sum' do
    subject { described_class.new.sum(a, b) }

    context 'when a is 1 and b is 2' do
      let(:a) { 1 }
      let(:b) { 2 }

      it 'returns summary' do
        is_expected.to eq(3)
      end
    end

    context 'when a is 100 and b is 200' do
      let(:a) { 100 }
      let(:b) { 200 }

      it 'returns summary' do
        is_expected.to eq(300)
      end
    end
  end

  describe '#subtract' do
    subject { described_class.new.subtract(a, b) }

    context 'when a is 1 and b is 2' do
      let(:a) { 1 }
      let(:b) { 2 }

      it 'raises an error' do
        expect { subject }.to raise_error(ArgumentError)
      end
    end

    context 'when a is 2 and b is 1' do
      let(:a) { 2 }
      let(:b) { 1 }

      it 'returns correct result' do
        is_expected.to eq(1)
      end
    end
  end
end
