require 'string_helper'

describe StringHelper do
  describe '#concatenate' do
    subject { described_class.new.concatenate(a, b) }

    context 'when a is git and b is lab' do
      let(:a) { 'git' }
      let(:b) { 'lab' }

      it 'returns summary' do
        is_expected.to eq('gitlab')
      end
    end

    context 'when a is git and b is 200' do
      let(:a) { 'git' }
      let(:b) { 200 }

      it 'raises an error' do
        expect { subject }.to raise_error(ArgumentError)
      end
    end
  end
end
