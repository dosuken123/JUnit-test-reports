import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class CalculatorTest {
  @Test
  public void evaluatesExpression() {
    Calculator calculator = new Calculator();
    int sum = calculator.evaluate("1+2+3");
    assertEquals(6, sum);
  }

  @Test
  public void sumExpression() {
    Calculator calculator = new Calculator();
    int sum = calculator.sum(1, 2);
    assertEquals(3, sum);
  }

  @Test
  public void subtractExpression() {
    Calculator calculator = new Calculator();
    int diff = calculator.subtract(2, 1);
    assertEquals(1, diff);
  }
}
